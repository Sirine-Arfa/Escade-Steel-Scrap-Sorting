#!/usr/bin/env python
# coding: utf-8

# # Step 1: Install Requirements

# In[1]:


#clone YOLOv5 and
#!git clone https://github.com/ultralytics/yolov5  # clone repo
%cd yolov5
%pip install -qr requirements.txt # install dependencies
%pip install -q roboflow

# In[1]:


import torch
import os
from IPython.display import Image, clear_output  # to display images

print(f"Setup complete. Using torch {torch.__version__} ({torch.cuda.get_device_properties(0).name if torch.cuda.is_available() else 'CPU'})")

# # Step2 : Assembeling the customized and labeled dataset with Roboflow:
# 

# In[3]:


%pip install roboflow
from roboflow import Roboflow
rf = Roboflow(api_key="zFaTuuKrKVuMYjgwvDVx")
project = rf.workspace("tu-dresden-fd0fk").project("whole-steel-scap-data")
dataset = project.version(1).download("yolov5")


# # Step 3: Train Our Custom YOLOv5 model
# 
# Here, we are able to pass a number of arguments:
# - **img:** define input image size
# - **batch:** determine batch size
# - **epochs:** define the number of training epochs. (Note: often, 3000+ are common here!)
# - **data:** Our dataset locaiton is saved in the `dataset.location`
# - **weights:** specify a path to weights to start transfer learning from. Here we choose the generic COCO pretrained checkpoint.
# - **cache:** cache images for faster training

# In[6]:


cd yolov5

# In[7]:


pwd

# In[8]:


!python train.py --img 416 --batch 16 --epochs 50 --data {dataset.location}/data.yaml --weights yolov5m.pt --cache

# # Evaluate Custom YOLOv5 Detector Performance
# Training losses and performance metrics are saved to Tensorboard and also to a logfile.

# In[9]:


# Start tensorboard
# Launch after you have started training
# logs save in the folder "runs"
%load_ext tensorboard


# In[10]:


%tensorboard --logdir runs

# # Run Inference  With Trained Weights
# Run inference with a pretrained checkpoint on contents of `test/images` folder downloaded from Roboflow.

# In[11]:


!python detect.py --weights runs/train/exp/weights/best.pt --img 416 --conf 0.1 --source {dataset.location}/test/images

# In[12]:


#display inference on ALL test images

import glob
from IPython.display import Image, display

for imageName in glob.glob('./runs/detect/exp/*.jpg'): #assuming JPG
    display(Image(filename=imageName))
    print("\n")

# # Model's perforrmance on the cutomized steel scrap dataset:

# 1. F1 Confidence Curve:

# In[13]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/F1_curve.png')
plt.figure(figsize=(15, 15), dpi=80)
plt.imshow(im)

# 2. Precision_Recall Curve:
# 
# 
# 

# In[14]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/PR_curve.png')
plt.figure(figsize=(15, 15), dpi=80)
plt.imshow(im)

# 2. Precision Curve:
# 

# In[15]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/P_curve.png')
plt.figure(figsize=(15, 15), dpi=80)
plt.imshow(im)

# 3. Recall Curve:

# In[16]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/R_curve.png')
plt.figure(figsize=(15, 15), dpi=80)
plt.imshow(im)

# 4. Confusion Matrix:

# In[17]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/confusion_matrix.png')
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(im)

# 4. Labels instances:

# In[18]:


from PIL import Image
import matplotlib.pyplot as plt
im = Image.open('./runs/train/exp/results.png')
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(im)

# In[19]:


import pandas as pd
results = pd.read_csv('./runs/train/exp/results.csv')

# In[20]:


results

# In[21]:


import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("./runs/train/exp/labels.jpg")
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(img_cvt)
plt.show()


# In[22]:


import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("./runs/train/exp/labels_correlogram.jpg")
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(img_cvt)
plt.show()

# In[5]:


# training resukts at batch 0
import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("./runs/train/exp/train_batch0.jpg")
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(img_cvt)
plt.show()

# In[24]:


import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("/content/yolov5/runs/train/exp/train_batch1.jpg")
plt.figure(figsize=(20, 20), dpi=80)
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img_cvt)
plt.show()

# In[25]:


import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("./runs/train/exp/train_batch2.jpg")
plt.figure(figsize=(20, 20), dpi=80)
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.imshow(img_cvt)
plt.show()

# In[26]:


import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread("./runs/train/exp/val_batch0_labels.jpg")
img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
plt.figure(figsize=(20, 20), dpi=80)
plt.imshow(img_cvt)
plt.show()

# In[4]:


#Model inference on a webcam (just run the cell, it will open the webcam for real time inference)
!python detect.py --weights runs/train/exp/weights/best.pt --img 416 --conf 0.1 --source 0

# In[32]:


#Model inference with a perexisting mp4 video
!python detect.py --weights runs/train/exp/weights/best.pt --img 416 --conf 0.1 --source datasets/steel-video.mp4

# In[29]:


#Checking the model's layers and parameters
import torch
from models.yolo import Model

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = Model(cfg='models/yolov5n.yaml', ch=3, nc=80).to(device)

# Print the model summary with layer names and output shapes
print("Layer (type)\t\t\t\tOutput Shape\t\t\tParam #")
print("==================================================================================")
total_params = 0
for name, param in model.named_parameters():
    if param.requires_grad:
        num_params = param.numel()
        total_params += num_params
        if param.dim() == 1:
            print(f"{name:<40}{param.size()}\t\t{num_params}")
        else:
            print(f"{name:<40}{list(param.size())}\t\t\t{num_params}")

print("==================================================================================")
print(f"Total params: {total_params}")

# In[30]:


#Loading the model's best weigths amd running the inference

import torch 
yolov5s = torch.hub.load('ultralytics/yolov5', 'custom', './runs/train/exp/weights/best.pt')
