# Steel Scrap sorting using the object detection model: 

YOLOV5 on the customized labeled and annotated DOES dataset

Please make sure to follow the notebook for training 

* This repository contains the following folders:

  - "datasets" : it contains the annotated dataset
  - "results from model" : contains the different metrics plots obtained after the training and inference of the model.
  - "yolov5" : it's empty and should exist once you run the notebook and clone the repo to your working directory (it's okey to delate it from your side).

* This repository contains the following files:

  - "yolov5_custom_training_Steel_Scrap_ESCADE.ipynb" : main jupyter notebook for running the training and inference.
  - "yolov5_custom_training_Steel_Scrap_ESCADE.py" : main python script for running the training and inference.
  - "yolov5m.pt" : the pretrained checkpoints of the yolov5 medium model on COCO dataset
  